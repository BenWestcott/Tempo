from PySide6.QtCore import QObject, Slot
from PySide6.QtQml import QmlElement
from PySide6.QtQuick import QQuickItem

QML_IMPORT_NAME = "io.tempo.pomodoro"
QML_IMPORT_MAJOR_VERSION = 1

pomodoro_backend = None
engine = None
post_init = None

@QmlElement
class PomodoroBridge(QObject):
	def __init__(self):
		super().__init__()
		pomodoro_backend.set_hook(self.__update_ui)
		post_init.append(self.post_init_handler)

	# Needs to be done after creation, as the engine has no root objects while it is being built
	def post_init_handler(self):
		# self.root is not used outside this function, but needs to be saved here
		# to prevent their C++ objects from getting garbage collected
		self.root = engine.rootObjects()[0]
		applet = self.root.findChild(QQuickItem, "pomodoroApplet")
		self.uiStatus = applet.findChild(QQuickItem, "pomodoroStatus")
		self.time = applet.findChild(QQuickItem, "pomodoroTime")
		self.progress = applet.findChild(QQuickItem, "pomodoroProgress")
		self.button = applet.findChild(QQuickItem, "pomodoroButton")
		assert(self.uiStatus and self.time and self.progress and self.button)
		self.__update_ui()

	def __update_ui(self):
		self.uiStatus.setProperty("text", pomodoro_backend.title)
		self.time.setProperty("text", pomodoro_backend.progress_msg)
		self.progress.setProperty("value", pomodoro_backend.progress_val)
		self.button.setProperty("text", pomodoro_backend.toggle_action)

	@Slot()
	def btnClicked(self):
		if pomodoro_backend.mode == 0:
			pomodoro_backend.start()
			# Use opacity over visibility so the positional space remains occupied and items don't move
			self.time.setProperty("opacity", 1)
			self.progress.setProperty("opacity", 1)
			self.__update_ui()
		else:
			pomodoro_backend.stop()
			self.time.setProperty("opacity", 0)
			self.progress.setProperty("opacity", 0)
			self.__update_ui()
