import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

ColumnLayout {
	id: clockApplet
	objectName: "clockApplet"
	property int timeTenths: 0
	property int localOffset: 0
	property string localName: "-"
	property int localTimeTenths: applytz(timeTenths, localOffset)
	Layout.preferredWidth: appletContainer.width
	Layout.preferredHeight: appletContainer.height

	function hours(tenths) {
		var hrs = Math.floor(tenths / 36000) % 12;
		if (hrs == 0) {
			hrs = 12;
		}
		return hrs;
	}
	function minutes(tenths) {
		return Math.floor(tenths / 600) % 60;
	}
	function seconds(tenths) {
		return Math.floor(tenths / 10) % 60;
	}
	function ispm(tenths) {
		return tenths >= 36000 * 13;
	}
	function applytz(tenths, hours) {
		var added = tenths + (hours * 36000);
		// % is remainder, not modulo, so make it into modulo
		return ((added % 864000) + 864000) % 864000;
	}
	function padnum(n) {
		return n.toString().padStart(2, "0");
	}
	function longtime(tenths) {
		return '' + hours(tenths) + ':' + padnum(minutes(tenths)) + ':' + padnum(seconds(tenths)) +
			'.' + tenths%10 + (ispm(tenths) ? ' PM' : ' AM');
	}
	function shorttime(tenths) {
		return '' + hours(tenths) + ':' + padnum(minutes(tenths)) + (ispm(tenths) ? ' PM' : ' AM');
	}
	function utcstr(tz) {
		var offset = Math.abs(tz);
		if (offset % 1 != 0) {
			offset = Math.floor(offset).toString() + ':' + (offset % 1) * 60;
		}
		return 'UTC' + (tz >= 0 ? '+' : '-') + offset;
	}
	function relstr(tz) {
		var offset = tz - clockApplet.localOffset;
		if (offset == 0) {
			return 'Local';
		} else if (offset > 0) {
			return '+' + offset + ' hours';
		} else {
			return '' + offset + ' hours';
		}
	}
	
	Button {
		id: clockAddOverlay
		parent: Overlay.overlay
		background: Rectangle {
			color: "#30000000"
		}
		visible: false
		width: parent.width
		height: parent.height
		onClicked: {
			visible = false
		}
		Rectangle {
			color: "white"
			width: 200
			height: 300
			x: (clockAddOverlay.width - width) / 2
			y: (clockAddOverlay.height - height) / 2
			Label {
				text: 'Zones: ' + clock.getZones().toString()
			}
		}
	}

	ClockAppletLocal {
		localTimeTenths: clockApplet.localTimeTenths
		localUtcOffset: clockApplet.localOffset
		localTzName: clockApplet.localName
		z: 1
	}

	ListModel {
		id: clockModel
		ListElement { name: "Los Angeles"; offset: -7 }
		ListElement { name: "New York"; offset: -4 }
		ListElement { name: "London"; offset: 0 }
		ListElement { name: "New Delhi"; offset: 5.5 }
		ListElement { name: "Kiritimati"; offset: 14 }
	}
	ScrollView {
		Layout.fillHeight: true
		Layout.fillWidth: true
		ScrollBar.vertical.policy: ScrollBar.AlwaysOn
		//visible: !clockAddOverlay.visible
		ListView {
			id: clockView
			model: clockModel
			objectName: "clockView"
			Layout.fillHeight: true // Without this, only shows one delegate
			delegate: Item { // Wrapper item seems to be needed, otherwise 'name' and 'offset' are not seen
				width: children[0].width
				height: children[0].height
				ClockAppletWorldItem {
					placeName: name
					timeTenthsUtc: clockApplet.timeTenths
					timeTenthsLocal: clockApplet.localTimeTenths
					thisUtcOffset: offset
					localUtcOffset: clockApplet.localOffset
					modelObj: clockView.model
					modelIndex: index
					width: clockView.width
				}
			}
		}
	}
	Rectangle {
		color: "white"
		//visible: !clockAddOverlay.visible
		Layout.fillWidth: true
		Layout.preferredHeight: clockAdd.height + 20
		Button {
			id: clockAdd
			text: "Add Clock"
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.bottom: parent.bottom
			onClicked: clockAddOverlay.visible = true
		}
	}
}
