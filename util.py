"""
@file util.py
@author Ben Westcott
@copyright Ben Westcott, 2023, BSD 3-clause
@brief Helper functions and classes not inherently associated with a particular file.
"""
from PySide6.QtCore import QPropertyAnimation

BTN_PLAIN = """
	QPushButton { border: none; background-color: none; padding: 10px; border-radius: 10px; }
	QPushButton:pressed { background-color: rgba(0, 0, 0, 30); }
"""

"""
@brief Converts a time period into a string of the format "%dh %dm %ds". Parts are omitted when zero.
@param secs The duration of the time perdiod, in seconds.
@return The duration as a Python string
"""
def secs_to_str(secs):
	s = secs % 60
	m = (secs - s) // 60
	h = (secs - m) // (60 * 60)
	str = ""
	addSpace = False
	if (h != 0):
		addSpace = True
		str += f"{h}h"
	if (m != 0):
		if addSpace:
			str += " "
		str += f"{m}m"
		addSpace = True
	if (s != 0):
		if addSpace:
			str += " "
		str += f"{s}s"
	return str

"""
@brief Concisely builds a QPropertyAnimation.
@param obj   The QObject to animate.
@param prop  The name of the property of that object to animate, as a Python string.
@param dur   The duration of the animation, in milliseconds.
@param start The initial value of the property.
@param end   The final value of the property.
@return The QPropertyAnimation. Call its start method to use it.
"""
def make_anim(obj, prop, dur, start, end):
	anim = QPropertyAnimation(obj, bytes(prop, 'utf-8'))
	anim.setDuration(dur)
	anim.setStartValue(start)
	anim.setEndValue(end)
	return anim

"""
@brief Centers an object using stretch instead of alignment.
       This allows alignment to control the way the widget is rendered when below minimum size.
@note Example usage: parent.addLayout(center_by_stretch(QHBoxLayout(), widget))
@param layout The layout to insert the widget into.
@param widget The widget to insert into the layout.
@return The provided layout.
"""
def center_by_stretch(layout, widget, stretch=0):
	layout.addStretch()
	layout.addWidget(widget, stretch)
	layout.addStretch()
	return layout
