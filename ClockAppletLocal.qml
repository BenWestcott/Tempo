import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Rectangle {
	id: clockLocalContainer
	required property int localTimeTenths
	required property int localUtcOffset
	required property string localTzName
	// Needed to cover overscan from the scrollview below
	color: "white"
	Layout.fillWidth: true
	Layout.preferredHeight: clockLocalLayout.height
	ColumnLayout {
		id: clockLocalLayout
		spacing: 0
		width: clockLocalContainer.width
		height: clockLocalTime.height + clockLocalInfo.height + clockSeparator.height + clockLocalTime.Layout.topMargin
		Label {
			id: clockLocalTime
			font.pointSize: 24
			font.family: "monospace"
			Layout.alignment: Qt.AlignHCenter
			Layout.topMargin: 20
			text: longtime(clockLocalContainer.localTimeTenths)
		}
		RowLayout {
			id: clockLocalInfo
			Layout.fillWidth: true
			Label {
				text: clockLocalContainer.localTzName
				font.pointSize: 16
			}
			Item { Layout.fillWidth: true }
			Label {
				text: utcstr(clockLocalContainer.localUtcOffset)
				font.pointSize: 12
			}
		}
		Rectangle {
			id: clockSeparator
			color: "#050505"
			Layout.fillWidth: true
			Layout.preferredHeight: 2
		}
	}
}
