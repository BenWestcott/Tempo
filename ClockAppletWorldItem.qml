import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

ColumnLayout {
	id: clockDelegate
	required property string placeName
	required property int timeTenthsUtc
	required property int timeTenthsLocal
	required property int thisUtcOffset
	required property int localUtcOffset
	required property var modelObj
	required property int modelIndex
	spacing: 0
	required width
	height: clockName.height + clockContainer.height + 20
	Label {
		id: clockName
		text: clockDelegate.placeName
		font.pointSize: 16
		Layout.topMargin: 10
		Layout.preferredWidth: clockContainer.width
		Layout.alignment: Qt.AlignHCenter
	}
	Rectangle {
		id: clockContainer
		color: "#18000000"
		radius: 20
		Layout.bottomMargin: 20
		Layout.preferredWidth:
			Math.max(
				clockTime.width + clockDate.width + clockInfo.width + 50,
				Math.min(
					clockApplet.width - 100,
					400
				)
			)
		Layout.preferredHeight: clockContent.height + (clockExtra.visible ? clockExtra.height : 0)
		Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
		ColumnLayout {
			spacing: 0
			width: clockContainer.width
			RowLayout {
				id: clockContent
				spacing: 0
				width: clockContainer.width
				Label {
					id: clockTime
					property int tenths: applytz(clockDelegate.timeTenthsUtc, clockDelegate.thisUtcOffset)
					text: shorttime(tenths)
					font.pointSize: 20
					Layout.leftMargin: clockContainer.radius * .5
					Layout.alignment: Qt.AlignVCenter
				}
				Label {
					id: clockDate
					text: {
						var hrs = clockDelegate.timeTenthsLocal / 36000
						var hrsleft = 24 - hrs
						var hrsahead = clockDelegate.thisUtcOffset - clockDelegate.localUtcOffset
						if ((hrsahead - hrsleft) > 0) {
							return "Tomorrow"
						} else if ((hrs + hrsahead) < 0) {
							return "Yesterday"
						} else {
							return ""
						}
					}
				}
				Item { Layout.fillWidth: true }
				Label {
					id: clockInfo
					text: relstr(clockDelegate.thisUtcOffset)
					font.pointSize: 12
					Layout.rightMargin: 10
					Layout.alignment: Qt.AlignRight
				}
				RoundButton {
					text: "v"
					flat: true
					radius: clockContainer.radius
					Layout.preferredWidth: height
					onClicked: {
						if (clockExtra.visible) {
							clockExtra.visible = false;
						} else {
							clockExtra.visible = true;
						}
					}
				}
			}
			RowLayout {
				id: clockExtra
				visible: false
				spacing: 0
				width: clockContent.width
				height: Math.max(clockExtraUtc.height, clockExtraDelete.height)
				Label {
					id: clockExtraUtc
					text: utcstr(clockDelegate.thisUtcOffset)
					font.pointSize: 12
					Layout.leftMargin: clockContainer.radius * .75
				}
				Item { Layout.fillWidth: true }
				RoundButton {
					id: clockExtraDelete
					text: "Remove"
					palette { button: "#30FF0000" }
					radius: clockContainer.radius
					onClicked: clockDelegate.modelObj.remove(clockDelegate.modelIndex)
				}
			}
		}
	}
}
