import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

RowLayout {
	id: drawerContainer
	width: window.width
	height: window.height
	Drawer {
		id: drawer
		width: 0.66 * drawerContainer.width
		height: drawerContainer.height
		ColumnLayout {
			Label {
				text: "Applets"
				font.pixelSize: 24
				Layout.alignment: Qt.AlignHCenter
			}
			ScrollView {
				id: drawerlist
				Layout.preferredWidth: drawer.width
				Layout.preferredHeight: drawer.height
				contentWidth: availableWidth
			}
		}
	}
	Button {
		id: drawerOpen
		text: ">"
		flat: true
		hoverEnabled: false
		Layout.preferredWidth: Qt.styleHints.startDragDistance
		Layout.fillHeight: true
		onClicked: drawer.open()
	}
	StackLayout {
		id: appletContainer
		currentIndex: 0
		Layout.fillWidth: true
		Layout.fillHeight: true
		ClockApplet{}
		PomodoroApplet {}
	}
	Item {
		// Padding so the StackLayout is centered
		Layout.preferredWidth: drawerOpen.width
	}
}
