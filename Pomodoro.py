"""
@file pomodoro.py
@author Ben Westcott
@copyright Ben Westcott, 2023, BSD 3-clause
@brief An applet that assists with the pomodoro technique for time management.
@note Auditory or visual notifications for state changes are not currently implemented.
"""
from PySide6.QtCore import QTimer
import util

class Pomodoro:
	def __init__(self, secs_work=25*60, secs_break=5*60, secs_rest=30*60, rest_interval=3):
	#def __init__(self, secs_work=10, secs_break=5, secs_rest=10, rest_interval=1):
		self.mode = 0
		self.mode_changed = False
		self.break_count = 0
		self.rest_interval = rest_interval
		self.time = 0
		self.max_work = secs_work
		self.max_break = secs_break
		self.max_rest = secs_rest
		self.max_current = 0

		self.title = 'Not Working'
		self.toggle_action = 'Start'
		self.progress_msg = ''
		self.progress_val = 0
		self.timer = QTimer()
		self.timer.setInterval(1000)
		self.timer.timeout.connect(self.__tick)
		self.hook = None

	"""Designates a function to be called every time internal state is updated."""
	def set_hook(self, callback):
		self.hook = callback

	"""Increments time, sets monitor variables, and calls state transitions as needed."""
	def __tick(self):
		if self.mode == 0:
			return
		self.time += 1
		self.progress_val = self.time / self.max_current
		self.progress_msg = util.secs_to_str(self.time) + " / " + util.secs_to_str(self.max_current)
		if self.time == self.max_current:
			if self.mode == 1:
				if self.break_count == self.rest_interval:
					self.__long_break()
				else:
					self.__short_break()
			else:
				self.__work()
		if self.hook:
			self.hook()

	"""Starts working from the stop state."""
	def start(self):
		self.break_count = 0
		self.__work()
		self.toggle_action = 'Stop'
		self.timer.start()

	"""Transitions to the stopped state. Ticks are ignored in this state. The only valid transition out is start."""
	def stop(self):
		self.timer.stop()
		self.mode = 0
		self.mode_changed = True
		self.title = 'Not Working'
		self.toggle_action = 'Start'

	"""Transitions to the working state."""
	def __work(self):
		self.mode = 1
		self.mode_changed = True
		self.time = 0
		self.title = 'Working'
		self.progress_msg = '0s / ' + util.secs_to_str(self.max_work)
		self.progress_pct = 0
		self.max_current = self.max_work

	"""Transitions to the short break state."""
	def __short_break(self):
		self.mode = 2
		self.mode_changed = True
		self.time = 0
		self.break_count += 1
		self.title = 'On Break'
		self.progress_msg = '0s / ' + util.secs_to_str(self.max_break)
		self.progress_pct = 0
		self.max_current = self.max_break

	"""Transitions to the long break state."""
	def __long_break(self):
		self.mode = 3
		self.mode_changed = True
		self.time = 0
		self.break_count = 0
		self.title = 'Long Break'
		self.progress_msg = '0s / ' + util.secs_to_str(self.max_rest)
		self.progress_pct = 0
		self.max_current = self.max_rest
