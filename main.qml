import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import io.tempo.clock
import io.tempo.pomodoro

ApplicationWindow {
	id: window
	visible: true
	title: "Tempo"

	ClockBridge { id: clock }
	PomodoroBridge { id: pomodoro }
	AppletDrawer {}
}
