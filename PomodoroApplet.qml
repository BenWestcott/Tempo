import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

ColumnLayout {
	id: pomodoroApplet
	objectName: "pomodoroApplet"
	Layout.preferredWidth: appletContainer.width
	Layout.preferredHeight: appletContainer.height
	Item {
		Layout.fillHeight: true
	}
	Label {
		objectName: "pomodoroStatus"
		font.pointSize: 24
		Layout.alignment: Qt.AlignHCenter
		bottomPadding: pomodoroApplet.height * .1
	}
	Label {
		objectName: "pomodoroTime"
		opacity: 0
		Layout.alignment: Qt.AlignHCenter
		bottomPadding: pomodoroApplet.height * .04
	}
	ProgressBar {
		objectName: "pomodoroProgress"
		opacity: 0
		Layout.preferredWidth: Math.min(400, pomodoroApplet.width-50)
		Layout.alignment: Qt.AlignHCenter
		bottomPadding: pomodoroApplet.height * .1
		bottomInset: bottomPadding // Fixes progress bar fill being misaligned with groove
	}
	Button {
		objectName: "pomodoroButton"
		Layout.alignment: Qt.AlignHCenter
		onClicked: pomodoro.btnClicked()
	}
	Item {
		Layout.fillHeight: true
	}
}
