from datetime import datetime
from dateutil import zoneinfo
from PySide6.QtCore import QObject, Slot, QTimer
from PySide6.QtQml import QmlElement
from PySide6.QtQuick import QQuickItem

QML_IMPORT_NAME = "io.tempo.clock"
QML_IMPORT_MAJOR_VERSION = 1

engine = None
post_init = None

@QmlElement
class ClockBridge(QObject):
	def __init__(self):
		super().__init__()
		post_init.append(self.post_init_handler)
		self.timer = QTimer()
		self.timer.setInterval(100)
		self.timer.timeout.connect(self.__tick)
		self.tzname = 'UTC'
		self.tzoffset = 0
		self.tzmap = zoneinfo.get_zonefile_instance().zones

	def post_init_handler(self):
		self.root = engine.rootObjects()[0]
		self.uiClock = self.root.findChild(QQuickItem, "clockApplet")
		assert(self.uiClock is not None)
		self.__update_tz(datetime.now().astimezone())
		self.timer.start()

	def __update_tz(self, time):
		offset = time.utcoffset().total_seconds() / 3600
		if offset != self.tzoffset:
			self.tzname = time.tzname()
			self.tzoffset = offset
			self.uiClock.setProperty("localOffset", self.tzoffset)
			self.uiClock.setProperty("localName", self.tzname)

	def __tick(self):
		time = datetime.now().astimezone()
		self.__update_tz(time)
		tenths = (time.microsecond // 100000) + (time.second * 10) + (time.minute * 600) + (time.utctimetuple().tm_hour * 36000)
		self.uiClock.setProperty("timeTenths", tenths)

	@Slot(result=list)
	def getZones(self):
		return list(self.tzmap.keys())
