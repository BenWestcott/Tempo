import sys
from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtQuickControls2 import QQuickStyle

import ClockBridge
import Pomodoro
import PomodoroBridge

if __name__ == "__main__":
	# Initialize applets
	post_init = []
	ClockBridge.post_init = post_init
	PomodoroBridge.pomodoro_backend = Pomodoro.Pomodoro()
	PomodoroBridge.post_init = post_init

	# Allocate UI
	app = QGuiApplication(sys.argv)
	QQuickStyle.setStyle("Basic") # See https://doc.qt.io/qt-6/qtquickcontrols-styles.html
	engine = QQmlApplicationEngine()

	# Prepare applets
	ClockBridge.engine = engine
	PomodoroBridge.engine = engine

	# Load UI
	engine.quit.connect(app.quit)
	engine.load('main.qml')
	if not engine.rootObjects():
		exit(1)

	# Applet post-ui setup
	for fn in post_init:
		fn()

	# Run
	exit(app.exec())
